name             'opsworks_rails_precompile'
maintainer       'Vindi'
maintainer_email 'icaro.seara@gmail.com'
license          'All rights reserved'
description      'Installs/Configures opsworks_rails_precompile'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'
